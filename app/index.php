<?php require_once "db.php";?>
<!DOCTYPE html>
<html>
<?php 
$title = 'IgorGames studio';
?>

<head>
    <meta charset="UTF-8">
    <title>
        <?php echo $title ?>
    </title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="shortcut icon" type="image/png" href="\img\favicon\apple-touch-icon.png"/>
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700,900" rel="stylesheet">
</head>

<body>
    <div class="main">
        <div class="main-overlay"></div>
        <div class="header">
            <div class="logo">
                <a href="/"><img src="img/logo.png" alt="logo"></a>
            </div>
            <div class="menu">
                <ul>
                    <li><a href="#" class="menu-link">Home</a></li>
                    <li><a href="#" class="menu-link">Why MISOCIAL</a></li>
                    <li><a href="#" class="menu-link">Features</a></li>
                    <li><a href="#" class="menu-link">Articles</a></li>
                    <li><a href="#" class="menu-link">Pricing</a></li>
                    <li><a href="#" class="get-started-btn menu-link">Get started</a></li>
                </ul>
            </div>
        </div>
        <div class="main-text">
            <span>social marketing & analystics</span>
            <h1>
                <?php echo $title ?>
            </h1>
            <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum</p>
        </div>
        <div><a href="#table" class="down-arrow">↓</a></div>
    </div>

    <div class="table" id="table">
        <div class="table-header">
            <span>MIsocial</span>
            <h2>Select a Plan</h2>
            <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum</p>
        </div>
    </div>

</body>

</html>
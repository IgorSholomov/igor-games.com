<?php
$driver = "mysql";
$host = "localhost";
$db_name = "dynweb";
$db_user = "mysql";
$db_pass = "mysql";
$charset = "utf8";
$option = [PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION];

$start = microtime(true); // Начало времени выполнения скрипта

try{
    $pdo = new PDO("$driver:host=$host;dbname=$db_name;charset=$charset", $db_user, $db_pass, $option);
}catch(PDOException $e){
    die("Не могу подключиться к базе данных .$e .");
}

$result = $pdo->query('SELECT * FROM films');

while($row = $result->fetch(PDO::FETCH_ASSOC)){

    $string = substr($row['description'], 0, 1000);
    $string = rtrim($string, "!,.-");
    $string = substr($string, 0, strrpos($string, ' '));

    echo '<b>'.$row['title']. '</b> - '.$row['duration']. ' минут. <br><i>'.$string.'... </i><br><br>';
}

$time = microtime(true) - $start; // Конец времени выполнения скрипта
echo $time;
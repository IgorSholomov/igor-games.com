var gulp         = require('gulp'),
	sass         = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	cleanCSS    = require('gulp-clean-css'),
	rename       = require('gulp-rename'),
	browserSync  = require('browser-sync').create(),
	concat       = require('gulp-concat'),
	uglify       = require('gulp-uglify');

gulp.task('browser-sync', function() {
		browserSync.init({
				server: {
				baseDir: "./app/"
			  },
			  port: 3001,
			  open: true,
			  notify: false
		});
});

gulp.task('sass', function(){
	gulp.src('./css/**/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({browsers: ['last 15 versions'], cascade: false}))
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.stream());
});

gulp.task('watch', function () {
	gulp.watch('./css/**/*.scss', ['sass']);
	//gulp.watch('app/libs/**/*.js', ['scripts']);
	gulp.watch('app/js/*.js').on("change", browserSync.reload);
	gulp.watch('app/*.html').on('change', browserSync.reload);
	gulp.watch('app/**/*.php').on('change', browserSync.reload);

});

gulp.task('default', ['browser-sync', 'watch']);
